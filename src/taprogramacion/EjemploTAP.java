import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.*;



/**
 * ACTION LISTENER ES UNA CLASE ABSTRACTA
 * CLASE ABSTRACTA: no se puede instanciar pero se pueden poner de subclase
 * INTERFAZ: igual que la clase abstracta PERO 
 * @author jvriv
 */
public class EjemploTAP implements ActionListener, MouseMotionListener, MouseListener { //ActionListener FORMA DOS

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        EjemploTAP objEjemplo = new EjemploTAP();
        
        objEjemplo.run();
        
        
    }
    
    
    public void run(){
    JFrame ventana = new JFrame("Mi primer programa");
    ventana.setLayout(new BorderLayout());
    ventana.setSize(300,200); //----> tamaño.
    ventana.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); //---> para que finalice cuando la cerremos
    
    JLabel etiqueta = new JLabel("Hola Mundo");
    JButton boton1 = new JButton ("Cerrar");
    JButton boton2 = new JButton ("Boton 2");
    JButton boton3 = new JButton ("Boton 3");
    
    
    ventana.add(etiqueta, BorderLayout.CENTER); 
    ventana.add(boton1, BorderLayout.SOUTH);
    ventana.add(boton2, BorderLayout.WEST);
    ventana.add(boton3, BorderLayout.EAST);
    
    
    /* FORMA 1
    MiControlador miCtrl = new MiControlador();
    boton1.addActionListener (miCtrl);
    boton2.addActionListener (miCtrl);
    boton3.addActionListener (miCtrl);
    
    */
    ventana.addMouseListener(new MouseAdapter(){
                                //ADAPTADOR
                                public void MouseClicked(MouseEvent me){
                                    System.out.println("Clicked on: "+me.getX()+" / "+me.getY()); 
                                   }
    });
    
    ventana.addMouseMotionListener(new MouseAdapter(){
                                //ADAPTADOR
                                public void MouseClicked(MouseEvent me){
                                    System.out.println("Clicked on: "+me.getX()+" / "+me.getY()); 
                                   }
    });
    ventana.addMouseMotionListener(this);
    
    //FORMA 2
    boton1.addActionListener(this);
    boton2.addActionListener(this);
    
    //FORMA 3 CLASE ANÓNIMA
    boton3.addActionListener( new ActionListener(){
                                    @Override
                                    public void actionPerformed(ActionEvent arg0) {
                                        System.out.println("Se presionó el botón 3");
                                    }
    });
    
    

    ventana.setVisible(true);
    }
    
    /* FORMA 1
    class MiControlador implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent arg0){
            System.out.println(arg0.toString());
            
            if(arg0.getActionCommand().equals("Cerrar")){ 
                System.exit(0);
            }
        }
    }
    */
    
    //FORMA 2
    @Override
    public void actionPerformed(ActionEvent arg0){
        System.out.println(arg0.toString());
        
        if(arg0.getActionCommand().equals("Cerrar")){
            System.exit(0);
        }
    }   

    //INTERFACES 
    
    @Override
    public void mouseDragged(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseMoved(MouseEvent arg0) {
        System.out.println("Motion "+arg0.getX()+" / "+arg0.getY());
    }

    @Override
    public void mouseClicked(MouseEvent arg0) {        
        System.out.println("Clicked on: "+arg0.getX()+" / "+arg0.getY());
    }

    @Override
    public void mousePressed(MouseEvent e) {
        
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        
    }

    @Override
    public void mouseExited(MouseEvent e) {
        
    }
}
